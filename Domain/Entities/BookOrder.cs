﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities {
    public class BookOrder {
        public BookOrder() { }
        public BookOrder(CartLine line, Order order) {
            BookId = line.Book.Id;
            Book = line.Book;
            BookCount = line.Quantity;
            OrderId = order.Id;
            Order = order;
        }
        public int BookId { get; set; }
        public Book Book { get; set; }
        public int BookCount { get; set; }
        public int OrderId { get; set; }
        public Order Order { get; set; }

        public decimal TotalValue() {
            return Book.Price * BookCount;
        }
    }
}
