﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities {
    public class Cart {
        private List<CartLine> lineCollection = new List<CartLine>();

        public void AddItem(Book game, int quantity) {
            CartLine line = lineCollection
                .Where(g => g.Book.Id == game.Id)
                .FirstOrDefault();

            if (line == null) {
                lineCollection.Add(new CartLine {
                    Book = game,
                    Quantity = quantity
                });
            } else {
                line.Quantity += quantity;
            }
        }

        public void RemoveItem(Book book) {
            CartLine line = lineCollection.Where(l => l.Book.Id == book.Id).First();
            line.Quantity--;
            if (line.Quantity == 0) {
                lineCollection.Remove(line);
            }
        }

        public decimal ComputeTotalValue() {
            return lineCollection.Sum(e => e.Book.Price * e.Quantity);

        }
        public void Clear() {
            lineCollection.Clear();
        }

        public IEnumerable<CartLine> Lines
        {
            get { return lineCollection; }
        }

        public int Count
        {
            get { return lineCollection.Sum(l => l.Quantity); }
        }
    }

    public class CartLine {
        public Book Book { get; set; }
        public int Quantity { get; set; }
    }
}
