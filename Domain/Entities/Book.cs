﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Domain.Entities {
    public class Book {

        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Display(Name = "Название")]
        [Required(ErrorMessage = "Введите название книги")]
        public string Name { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Автор")]
        [Required(ErrorMessage = "Укажите автора")]
        public string Author { get; set; }

        [Display(Name = "Описание")]
        [Required(ErrorMessage = "Добавьте описание")]
        public string Description { get; set; }

        [Display(Name = "Жанр")]
        [Required(ErrorMessage = "Укажите жанр")]
        public string Genre { get; set; }

        [Display(Name = "Цена")]
        [Required(ErrorMessage = "Укажите цену")]
        public decimal Price { get; set; }

        [Display(Name = "Изображение")]
        public byte[] ImageData { get; set; }

        public string ImageMimeType { get; set; }
    }
}
