﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities {
    public class Order {
        public Order() {}
        public Order(ShippingDetails details) {
            Name = details.Name;
            Email = details.Email;
            Phone = details.Phone;
            Country = details.Country;
            City = details.City;
            Address = details.Address;
            Date = DateTime.Now;
            BookOrders = new List<BookOrder>();
        }
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public List<BookOrder> BookOrders { get; set; }

        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Address { get; set; }

        public decimal TotalValue() {
            return BookOrders.Sum(b => b.TotalValue());
        }
    }
}
