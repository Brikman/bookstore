﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Abstract;
using Domain.Entities;

namespace Domain.Concrete {
    public class BookRepository : IBookRepository {
        ApplicationContext context;

        public BookRepository(ApplicationContext context) {
            this.context = context;
        }

        public IEnumerable<Book> Books
        {
            get { return context.Books; }
        }

        public void SaveBook(Book book) {
            if (book.Id == 0) {
                book.Genre = book.Genre.ToLower();
                context.Books.Add(book);
            } else {
                Book dbEntry = context.Books.Find(book.Id);
                if (dbEntry != null) {
                    dbEntry.Name = book.Name;
                    dbEntry.Description = book.Description;
                    dbEntry.Price = book.Price;
                    dbEntry.Genre = book.Genre.ToLower();
                    dbEntry.ImageData = book.ImageData;
                    dbEntry.ImageMimeType = book.ImageMimeType;
                }
            }
            context.SaveChanges();
        }

        public Book DeleteBook(int bookId) {
            Book dbEntry = context.Books.Find(bookId);
            if (dbEntry != null) {
                context.Books.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }
    }
}
