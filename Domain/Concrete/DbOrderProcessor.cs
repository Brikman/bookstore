﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Abstract;
using Domain.Entities;

namespace Domain.Concrete {
    public class DbOrderProcessor : IOrderProcessor {
        IOrderRepository repository;

        public DbOrderProcessor(IOrderRepository repo) {
            repository = repo;
        }

        public void ProcessOrder(Cart cart, ShippingDetails details) {
            Order order = new Order(details);
            foreach (CartLine line in cart.Lines) {
                order.BookOrders.Add(new BookOrder(line, order));
            }
            repository.SaveOrder(order);
        }
    }
}
