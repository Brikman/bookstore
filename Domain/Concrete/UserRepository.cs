﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Abstract;
using Domain.Entities;

namespace Domain.Concrete {
    public class UserRepository : IUserRepository {
        ApplicationContext context;

        public UserRepository(ApplicationContext context) {
            this.context = context;
        }

        public IEnumerable<User> Users
        {
            get { return context.Users; }
        }

        public User DeleteUser(int userId) {
            throw new NotImplementedException();
        }

        public void RegisterUser(User user) {
            throw new NotImplementedException();
        }
    }
}
