﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Abstract;
using Domain.Entities;

namespace Domain.Concrete {
    public class OrderRepository : IOrderRepository {
        ApplicationContext context;

        public OrderRepository(ApplicationContext context) {
            this.context = context;
        }

        public IEnumerable<Order> Orders
        {
            get
            {
                List<Order> orders = context.Orders.ToList();
                foreach (Order order in orders) {
                    order.BookOrders = context.BookOrder.Where(bo => bo.OrderId == order.Id).ToList();
                }
                return orders;
            }
        }

        public void SaveOrder(Order order) {
            if (order.Id == 0) {
                context.Orders.Add(order);
            } else {
                Order dbEntry = context.Orders.Find(order.Id);
                if (dbEntry != null) {
                    dbEntry.Name = order.Name;
                    dbEntry.Email = order.Email;
                    dbEntry.Phone = order.Phone;
                    dbEntry.Country = order.Country;
                    dbEntry.City = order.City;
                    dbEntry.Address = order.Address;
                    dbEntry.BookOrders = order.BookOrders;
                }
            }
            context.SaveChanges();
        }

        public Order DeleteOrder(int orderId) {
            Order dbEntry = context.Orders.Find(orderId);
            if (dbEntry != null) {
                context.Orders.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }
    }
}
