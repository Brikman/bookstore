﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Abstract;
using System.Web.Security;
using Domain.Entities;

namespace Domain.Concrete {
    public class FormAuthProvider : IAuthProvider {
        public bool Authenticate(string username, string password, UserRole requiredRole) {
            //UserRepository repository = new UserRepository(new ApplicationContext());
            //User user = repository.Users
            //    .Where(u => u.Login == username &&
            //                u.Password == password &&
            //                u.Role == requiredRole)
            //    .FirstOrDefault();
            
            bool result = FormsAuthentication.Authenticate(username, password);
            if (result)
                FormsAuthentication.SetAuthCookie(username, false);
            return result;
        }
    }
}
