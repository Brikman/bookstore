﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Abstract;
using Domain.Entities;
using System.Web.Security;

namespace Domain.Concrete {
    public class AuthProvider : IAuthProvider {
        IUserRepository repository;

        public AuthProvider(IUserRepository repo) {
            repository = repo;
        }

        public bool Authenticate(string username, string password, UserRole requiredRole) {
            User user = repository.Users
                .Where(u => u.Login == username &&
                            u.Password == password &&
                            u.Role == requiredRole)
                .FirstOrDefault();
            bool result = user != null;
            if (result)
                FormsAuthentication.SetAuthCookie(username, false);
            return result;
        }
    }
}
