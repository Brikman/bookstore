﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Entities;

namespace Domain.Concrete {
    public class ApplicationContext : DbContext {
        public DbSet<Book> Books { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<BookOrder> BookOrder { get; set; }  
        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            modelBuilder.Entity<BookOrder>()
                .HasKey(bc => new { bc.BookId, bc.OrderId });

            modelBuilder.Entity<BookOrder>()
                .HasOne(bc => bc.Order)
                .WithMany(b => b.BookOrders)
                .HasForeignKey(bc => bc.OrderId);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
            optionsBuilder.UseNpgsql(
                "Host=localhost;" +
                "Port=5432;" +
                "Database=bookstore;" +
                "Username=brikman;" +
                "Password=123");
        }
    }
}
