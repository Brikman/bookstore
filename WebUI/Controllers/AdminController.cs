﻿using Domain.Abstract;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebUI.Controllers {
    [Authorize]
    public class AdminController : Controller {
        IBookRepository bookRepository;
        IOrderRepository orderRepository;

        public AdminController(IBookRepository bookRepo, IOrderRepository orderRepo) {
            bookRepository = bookRepo;
            orderRepository = orderRepo;
        }

        public ViewResult CreateBook() {
            return View("EditBook", new Book());
        }

        public ViewResult EditBook(int bookId) {
            Book book = bookRepository.Books
                .FirstOrDefault(g => g.Id == bookId);
            return View(book);
        }

        [HttpPost]
        public ActionResult EditBook(Book book, HttpPostedFileBase image = null) {
            if (ModelState.IsValid) {
                if (image != null) {
                    book.ImageMimeType = image.ContentType;
                    book.ImageData = new byte[image.ContentLength];
                    image.InputStream.Read(book.ImageData, 0, image.ContentLength);
                }
                bookRepository.SaveBook(book);
                TempData["message"] = string.Format("Изменения в игре \"{0}\" были сохранены", book.Name);
                return RedirectToAction("Index");
            } else {
                return View(book);
            }
        }

        [HttpPost]
        public ActionResult DeleteBook(int bookId) {
            Book deletedBook = bookRepository.DeleteBook(bookId);
            if (deletedBook != null) {
                TempData["message"] = string.Format("Книга \"{0}\" была удалена", deletedBook.Name);
            }
            return RedirectToAction("Index");
        }

        public ViewResult Index() {
            return View(bookRepository.Books);
        }

        public ViewResult ListOrders() {
            return View(orderRepository.Orders);
        }

        public ViewResult DeleteOrder(int orderId) {
            Order deletedOrder = orderRepository.DeleteOrder(orderId);
            if (deletedOrder != null) {
                TempData["message"] = string.Format("Заказ №{0} был удалён", deletedOrder.Id);
            }
            return View("ListOrders", orderRepository.Orders);
        }
    }
}