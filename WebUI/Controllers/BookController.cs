﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Abstract;
using Domain.Entities;
using WebUI.Models;

namespace WebUI.Controllers {
    public class BookController : Controller {
        private IBookRepository repository;
        public int pageSize = 4;

        public BookController(IBookRepository repo) {
            repository = repo;
        }

        public ViewResult List(string genre, int page = 1) {
            if (genre != null)
                genre = genre.ToLower();

            IEnumerable<Book> books = repository.Books
                .Where(p => genre == null || p.Genre == genre);

            BookListViewModel model = new BookListViewModel {
                Books = books
                    .OrderBy(game => game.Id)
                    .Skip((page - 1) * pageSize)
                    .Take(pageSize),
                PagingInfo = new PagingInfo {
                    CurrentPage = page,
                    ItemsPerPage = pageSize,
                    TotalItems = books.Count()
                },
                CurrentGenre = genre
            };
            return View(model);
        }

        [HttpPost]
        public ViewResult List(BookListViewModel model) {
            if (model == null)
                return List(null, 1);
            
            string search = model.SearchLine;
            string genre = model.CurrentGenre;
            if (search != null)
                search = search.ToLower();
            if (genre != null)
                genre = genre.ToLower();

            IEnumerable<Book> books = repository.Books
                .Where(p => genre == null || p.Genre.ToLower() == genre)
                .Where(p => search == null ||
                       p.Name.ToLower().Contains(search) || p.Author.ToLower().Contains(search));

            model = new BookListViewModel {
                Books = books
                    .OrderBy(game => game.Id)
                    .Take(pageSize),
                PagingInfo = new PagingInfo {
                    CurrentPage = 1,
                    ItemsPerPage = pageSize,
                    TotalItems = books.Count()
                },
                CurrentGenre = genre,
                SearchLine = search
            };
            return View(model);
        }

        public FileContentResult GetImage(int bookId) {
            Book book = repository.Books
                .FirstOrDefault(b => b.Id == bookId);

            if (book != null) {
                return File(book.ImageData, book.ImageMimeType);
            } else {
                return null;
            }
        }
    }
}