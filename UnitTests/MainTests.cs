﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Domain.Abstract;
using Domain.Entities;
using WebUI.Controllers;
using WebUI.Models;
using WebUI.HtmlHelpers;

namespace UnitTests {
    [TestClass]
    public class MainTests {
        [TestMethod]
        public void Can_Paginate() {
            // Организация (arrange)
            Mock<IBookRepository> mock = new Mock<IBookRepository>();
            mock.Setup(m => m.Books).Returns(new List<Book>
            {
                new Book { Id = 1, Name = "книга1"},
                new Book { Id = 2, Name = "книга2"},
                new Book { Id = 3, Name = "книга3"},
                new Book { Id = 4, Name = "книга4"},
                new Book { Id = 5, Name = "книга5"}
            });
            BookController controller = new BookController(mock.Object);
            controller.pageSize = 3;

            // Действие (act)
            BookListViewModel result = (BookListViewModel)controller.List(null, 2).Model;

            // Утверждение
            List<Book> Books = result.Books.ToList();
            Assert.IsTrue(Books.Count == 2);
            Assert.AreEqual(Books[0].Name, "книга4");
            Assert.AreEqual(Books[1].Name, "книга5");
        }

        [TestMethod]
        public void Can_Generate_Page_Links() {
            HtmlHelper myHelper = null;

            PagingInfo pagingInfo = new PagingInfo {
                CurrentPage = 2,
                TotalItems = 28,
                ItemsPerPage = 10
            };

            Func<int, string> pageUrlDelegate = i => "Page" + i;

            MvcHtmlString result = myHelper.PageLinks(pagingInfo, pageUrlDelegate);

            Assert.AreEqual(@"<a class=""btn btn-default"" href=""Page1"">1</a>"
                + @"<a class=""btn btn-default btn-primary selected"" href=""Page2"">2</a>"
                + @"<a class=""btn btn-default"" href=""Page3"">3</a>",
                result.ToString());
        }

        [TestMethod]
        public void Can_Send_Pagination_View_Model() {
            Mock<IBookRepository> mock = new Mock<IBookRepository>();
            mock.Setup(m => m.Books).Returns(new List<Book>
            {
                new Book { Id = 1, Name = "книга1"},
                new Book { Id = 2, Name = "книга2"},
                new Book { Id = 3, Name = "книга3"},
                new Book { Id = 4, Name = "книга4"},
                new Book { Id = 5, Name = "книга5"}
            });
            BookController controller = new BookController(mock.Object);
            controller.pageSize = 3;

            BookListViewModel result = (BookListViewModel)controller.List(null, 2).Model;

            PagingInfo pageInfo = result.PagingInfo;
            Assert.AreEqual(pageInfo.CurrentPage, 2);
            Assert.AreEqual(pageInfo.ItemsPerPage, 3);
            Assert.AreEqual(pageInfo.TotalItems, 5);
            Assert.AreEqual(pageInfo.TotalPages, 2);
        }

        [TestMethod]
        public void Can_Filter_Books() { 
            Mock<IBookRepository> mock = new Mock<IBookRepository>();
            mock.Setup(m => m.Books).Returns(new List<Book>
            {
                new Book { Id = 1, Name = "книга1", Genre = "жанр1"},
                new Book { Id = 2, Name = "книга2", Genre = "жанр2"},
                new Book { Id = 3, Name = "книга3", Genre = "жанр3"},
                new Book { Id = 4, Name = "книга4", Genre = "жанр2"},
                new Book { Id = 5, Name = "книга5", Genre = "жанр5"}
            });
            BookController controller = new BookController(mock.Object);
            controller.pageSize = 3;

            List<Book> result = ((BookListViewModel)controller.List("жанр2", 1).Model)
                .Books.ToList();

            Assert.AreEqual(result.Count(), 2);
            Assert.IsTrue(result[0].Name == "книга2" && result[0].Genre == "жанр2");
            Assert.IsTrue(result[1].Name == "книга4" && result[1].Genre == "жанр2");
        }

        [TestMethod]
        public void Can_Create_Genres() {
            Mock<IBookRepository> mock = new Mock<IBookRepository>();
            mock.Setup(m => m.Books).Returns(new List<Book> {
                new Book { Id = 1, Name = "книга1", Genre="роман"},
                new Book { Id = 2, Name = "книга2", Genre="триллер"},
                new Book { Id = 3, Name = "книга3", Genre="фантастика"},
                new Book { Id = 4, Name = "книга4", Genre="роман"},
            });

            NavController target = new NavController(mock.Object);

            List<string> results = ((IEnumerable<string>)target.Menu().Model).ToList();

            Assert.AreEqual(results.Count(), 3);
            Assert.AreEqual(results[0], "роман");
            Assert.AreEqual(results[1], "триллер");
            Assert.AreEqual(results[2], "фантастика");
        }

        [TestMethod]
        public void Indicates_Selected_Genre() {
            Mock<IBookRepository> mock = new Mock<IBookRepository>();
            mock.Setup(m => m.Books).Returns(new Book[] {
                new Book { Id = 1, Name = "книга1", Genre="роман"},
                new Book { Id = 2, Name = "книга2", Genre="триллер"}
            });

            NavController controller = new NavController(mock.Object);

            string selectedGenre = "триллер";

            string result = controller.Menu(selectedGenre).ViewBag.SelectedGenre;

            Assert.AreEqual(selectedGenre, result);
        }

        [TestMethod]
        public void Generate_Genre_Specific_Book_Count() {
            Mock<IBookRepository> mock = new Mock<IBookRepository>();
            mock.Setup(m => m.Books).Returns(new List<Book>
            {
                new Book { Id = 1, Name = "книга1", Genre="роман"},
                new Book { Id = 2, Name = "книга2", Genre="триллер"},
                new Book { Id = 3, Name = "книга3", Genre="роман"},
                new Book { Id = 4, Name = "книга4", Genre="триллер"},
                new Book { Id = 5, Name = "книга5", Genre="рассказ"}
            });
            BookController controller = new BookController(mock.Object);
            controller.pageSize = 3;

            int res1 = ((BookListViewModel)controller.List("роман").Model).PagingInfo.TotalItems;
            int res2 = ((BookListViewModel)controller.List("триллер").Model).PagingInfo.TotalItems;
            int res3 = ((BookListViewModel)controller.List("рассказ").Model).PagingInfo.TotalItems;
            int resAll = ((BookListViewModel)controller.List(null, 1).Model).PagingInfo.TotalItems;

            Assert.AreEqual(res1, 2);
            Assert.AreEqual(res2, 2);
            Assert.AreEqual(res3, 1);
            Assert.AreEqual(resAll, 5);
        }
    }
}
