﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Domain.Abstract;
using Domain.Entities;
using WebUI.Controllers;
using System.Linq;
using System.Web.Mvc;
using System.Collections.Generic;

namespace UnitTests {
    [TestClass]
    public class ImageTests {
        [TestMethod]
        public void Can_Retrieve_Image_Data() {
            Book game = new Book {
                Id = 2,
                Name = "книга2",
                ImageData = new byte[] { },
                ImageMimeType = "image/png"
            };

            Mock<IBookRepository> mock = new Mock<IBookRepository>();
            mock.Setup(m => m.Books).Returns(new List<Book> {
                new Book {Id = 1, Name = "книга1"},
                game,
                new Book {Id = 3, Name = "книга3"}
            }.AsQueryable());

            BookController controller = new BookController(mock.Object);

            ActionResult result = controller.GetImage(2);

            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(FileResult));
            Assert.AreEqual(game.ImageMimeType, ((FileResult)result).ContentType);
        }

        [TestMethod]
        public void Cannot_Retrieve_Image_Data_For_Invalid_ID() {
            Mock<IBookRepository> mock = new Mock<IBookRepository>();
            mock.Setup(m => m.Books).Returns(new List<Book> {
                new Book {Id = 1, Name = "книга1"},
                new Book {Id = 2, Name = "книга2"}
            }.AsQueryable());

            BookController controller = new BookController(mock.Object);

            ActionResult result = controller.GetImage(10);

            Assert.IsNull(result);
        }
    }
}
