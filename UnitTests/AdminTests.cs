﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Domain.Abstract;
using Domain.Entities;
using WebUI.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace UnitTests {
    [TestClass]
    public class AdminTests {
        Mock<IOrderRepository> orderRepo = new Mock<IOrderRepository>();

        [TestMethod]
        public void Index_Contains_All_Books() {
            Mock<IBookRepository> bookRepo = new Mock<IBookRepository>();
            bookRepo.Setup(m => m.Books).Returns(new List<Book>
            {
                new Book { Id = 1, Name = "книга1"},
                new Book { Id = 2, Name = "книга2"},
                new Book { Id = 3, Name = "книга3"},
                new Book { Id = 4, Name = "книга4"},
                new Book { Id = 5, Name = "книга5"}
            });

            AdminController controller = new AdminController(bookRepo.Object, orderRepo.Object);

            List<Book> result = ((IEnumerable<Book>)controller.Index().
                ViewData.Model).ToList();

            Assert.AreEqual(result.Count(), 5);
            Assert.AreEqual("книга1", result[0].Name);
            Assert.AreEqual("книга2", result[1].Name);
            Assert.AreEqual("книга3", result[2].Name);
        }

        [TestMethod]
        public void Can_Edit_Book() {
            Mock<IBookRepository> bookRepo = new Mock<IBookRepository>();
            bookRepo.Setup(m => m.Books).Returns(new List<Book>
            {
                new Book { Id = 1, Name = "книга1"},
                new Book { Id = 2, Name = "книга2"},
                new Book { Id = 3, Name = "книга3"},
                new Book { Id = 4, Name = "книга4"},
                new Book { Id = 5, Name = "книга5"}
            });

            AdminController controller = new AdminController(bookRepo.Object, orderRepo.Object);

            Book book1 = controller.EditBook(1).ViewData.Model as Book;
            Book book2 = controller.EditBook(2).ViewData.Model as Book;
            Book book3 = controller.EditBook(3).ViewData.Model as Book;

            Assert.AreEqual(1, book1.Id);
            Assert.AreEqual(2, book2.Id);
            Assert.AreEqual(3, book3.Id);
        }

        [TestMethod]
        public void Cannot_Edit_Nonexistent_Book() {
            Mock<IBookRepository> bookRepo = new Mock<IBookRepository>();
            bookRepo.Setup(m => m.Books).Returns(new List<Book>
            {
                new Book { Id = 1, Name = "книга1"},
                new Book { Id = 2, Name = "книга2"},
                new Book { Id = 3, Name = "книга3"},
                new Book { Id = 4, Name = "книга4"},
                new Book { Id = 5, Name = "книга5"}
            });

            AdminController controller = new AdminController(bookRepo.Object, orderRepo.Object);

            Book result = controller.EditBook(6).ViewData.Model as Book;

            Assert.IsNull(result);
        }

        [TestMethod]
        public void Can_Save_Valid_Changes() {
            Mock<IBookRepository> bookRepo = new Mock<IBookRepository>();

            AdminController controller = new AdminController(bookRepo.Object, orderRepo.Object);

            Book book = new Book { Name = "Test" };

            ActionResult result = controller.EditBook(book, null);

            bookRepo.Verify(m => m.SaveBook(book));

            Assert.IsNotInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void Cannot_Save_Invalid_Changes() {
            Mock<IBookRepository> bookRepo = new Mock<IBookRepository>();

            AdminController controller = new AdminController(bookRepo.Object, orderRepo.Object);

            Book book = new Book { Name = "Test" };

            controller.ModelState.AddModelError("error", "error");

            ActionResult result = controller.EditBook(book, null);

            bookRepo.Verify(m => m.SaveBook(It.IsAny<Book>()), Times.Never());

            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void Can_Delete_Valid_Books() { 
            Book book = new Book { Id = 2, Name = "книга2" };

            Mock<IBookRepository> bookRepo = new Mock<IBookRepository>();
            bookRepo.Setup(m => m.Books).Returns(new List<Book>
            {
                new Book { Id = 1, Name = "книга1"},
                new Book { Id = 2, Name = "книга2"},
                new Book { Id = 3, Name = "книга3"},
                new Book { Id = 4, Name = "книга4"},
                new Book { Id = 5, Name = "книга5"}
            });

            AdminController controller = new AdminController(bookRepo.Object, orderRepo.Object);

            controller.DeleteBook(book.Id);

            bookRepo.Verify(m => m.DeleteBook(book.Id));
        }
    }
}
